#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>

#define LIMIT 20161

int sum_proper_divs(int n) {
	int sum = 0;

	int i = 1;
	int lim = (int) ceil(sqrt(n));
	for(; i<lim; ++i)
		if(n%i == 0)
			sum += i;
	return sum;
}

int main(void) {
	int i, j;
	int sum = 0;
	int abun_len = 5000; /* Precalculated, it prob.. definitely won't
				for a greater LIMIT (greater than 20161) */
	int *abun = malloc(sizeof(int)*abun_len);
	int *nums = malloc(sizeof(int)*LIMIT);
	
	int *tmp = abun;
	for(i=1; i<=LIMIT; ++i) {
		if(i < sum_proper_divs(i)) {
			*tmp++ = i;
		}
		nums[i-1] = i;
	}

	abun_len = tmp - abun-1;

	for(i=0; i<abun_len; ++i)
		for(j=i; j<abun_len; ++j) {
			if(abun[i]+abun[j] <= LIMIT)
				nums[abun[i]+abun[j]-1] = 0;
		}
	for(i=0; i<LIMIT; ++i)
		sum += nums[i];

	printf("Sum numbers not made from two abundant numbers: %d\n", sum);
	return EXIT_SUCCESS;
}
