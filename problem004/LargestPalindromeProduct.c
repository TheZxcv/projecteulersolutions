#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int checkPalindromeNum(int n) {
	int num = n;
	int rev = 0, tmp;
	/* reverse number */
	while(num > 0) {
		tmp = num%10;
		rev = rev*10 + tmp;
		num /= 10;
	}
	return (n == rev);
}

int main(void) {
	int n = 99*91;
	printf("%d: %d\n", n, checkPalindromeNum(n));
	int i, j;
	int mI, mJ, max = 0;
	for(i=90; i > 9; i--) {
		for(j=999; j > 99; --j) {
			if(checkPalindromeNum(i*11*j))
				break;
		}
		if(i*11*j > max) {
			max = i*11*j;
			mI = i*11; mJ = j;
		}
	}
	printf("i:%d, j:%d\n", mI, mJ);
	printf("Pal: %d\n", max);
	return EXIT_SUCCESS;
}
