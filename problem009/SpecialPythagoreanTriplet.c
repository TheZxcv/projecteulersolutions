#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(void) {
	int a, b, c, sumSquares;
	int found = 0;
	for(a = 1; a<500 && !found; a++)
		for(b = 1; b<500 && !found; b++) {
			sumSquares = a*a + b*b;
			c = (int) ceil(sqrt(sumSquares));
			if(c*c == sumSquares)
				if(a+b+c == 1000) {
					printf("%d*%d*%d=%d\n", a,b,c,a*b*c);
					found = 1;
				}
		}			
	return EXIT_SUCCESS;
}
