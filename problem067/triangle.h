#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <stdio.h>
#include <stdlib.h>

#define TRUE 1
#define FALSE 0

#define SUCCESS 0
#define FAILURE 1

#define TRIANGLE_LINE_INITIAL_SIZE 10
#define TRIANGLE_LINE_STEP 4

#define TRIANGLELINE_NODE_INITIAL_SIZE 10
#define TRIANGLELINE_NODE_STEP 4

struct Triangle {
	struct TriangleLine **line;
	int lines;
	int _len;
};

struct TriangleLine {
	struct TriangleNode *node;
	int nodes;
	int _len;
};

struct TriangleNode {
	struct TriangleNode *childL;
	struct TriangleNode *childR;
	int value;
};

struct TriangleLine *newTrianLine(int len);
struct Triangle *newTriangle(void);

void freeTriangle(struct Triangle *trian);
void freeTriangleLine(struct TriangleLine *line);

int addNode(struct TriangleLine *line, int value);
int addLine(struct Triangle *tri, struct TriangleLine *line);

#endif
