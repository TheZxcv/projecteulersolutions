#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// somma estremi di un quadrato: 4n^2 - 6(n+1)
// ci sono solo numeri dispari
// sum from k=1 to n of (4(2k+1)^2 - 6(2k+1-1))

int main(void) {
	int i = 1001;
	if(i%2) { /* must be odd */
		int odds = (i-1)/2;
		long sum = 0;
		sum = 2*(8*odds*odds*odds + 15*odds*odds+13*odds);
		sum /= 3;
		sum++;
		printf("%ld\n", sum);
	}
	return EXIT_SUCCESS;
}
