#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT 10000

int sum_proper_divs(int n) {
	int sum = 0;

	int i = 1;
	for(; i<n/2+1; ++i)
		if(n%i == 0)
			sum += i;
	return sum;
}

int main(void) {
	int sum = 0;
	int num = 220;
	for(; num < LIMIT; ++num) {
		int tmp = sum_proper_divs(num);
		if(tmp < LIMIT && tmp > num && sum_proper_divs(tmp) == num)
			sum += tmp + num;
//		if(tmp > num)
//			num = tmp;
	}
	
	printf("Sum: %d\n", sum);

	return EXIT_SUCCESS;
}
