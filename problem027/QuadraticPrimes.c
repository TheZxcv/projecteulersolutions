#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0
#define PRIME_LESS_THAN_1000 169

int isPrime(int n) {
	if(n < 0) n=-n;
	int lim = (int) ceil(sqrt(n));
	int i;
	if(n%2 == 0)
		return ((n==2) ? TRUE : FALSE);
	for(i = 3; i <= lim; i += 2)
		if(n%i == 0)
			return FALSE;
	return TRUE;
}

int countPrimeSeq(int p, int q) {
	int n=1;
	while(isPrime(n*n+p*n+q))
		n++;
	return n;
}

int main(void) {
	int *firstPrimes = malloc(PRIME_LESS_THAN_1000*sizeof(int));
	int i,j,k=0;
	int *temp = firstPrimes;
	for(i=0; i<1000; ++i) {
		if(isPrime(i))
			*temp++ = i, k++;
	}

	int p, q, bestP, bestQ, seq, bestSeq=0;
	for(i = 0; i<PRIME_LESS_THAN_1000; ++i) {
		p = firstPrimes[i];
		for(k = 0; k<PRIME_LESS_THAN_1000; ++k) {
			q = firstPrimes[k];
			if(i==k) continue;
			for(j=0; j<4; ++j) {
				if(j == 1 || j == 3)
					p = -p;
				if(j == 0 || j == 2)
					q = -q;
				if(isPrime(p+q+1)) {
					seq = countPrimeSeq(p, q);
					if(seq > bestSeq) {
						bestSeq = seq;
						bestP = p;
						bestQ = q;
					}
				}
			}
		}
	}

	printf("p=%d,q=%d,seq=%d,a*b=%d\n", bestP, bestQ, bestSeq, bestP*bestQ);
	
	return EXIT_SUCCESS;
}
