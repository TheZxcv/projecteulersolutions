#include "triangle.h"

struct TriangleLine *newTrianLine(int len) {
	if(len < 1)
		len = TRIANGLELINE_NODE_INITIAL_SIZE;

	struct TriangleLine *lin = malloc(sizeof(struct TriangleLine));
	lin->_len = len;
	lin->node = malloc(sizeof(struct TriangleNode)*len);
	lin->nodes = 0;
	
	return lin;
}

struct Triangle *newTriangle(void) {
	struct Triangle *tri = malloc(sizeof(struct Triangle));
	tri->_len = TRIANGLE_LINE_INITIAL_SIZE;
	tri->line = malloc(sizeof(struct TriangleLine *)*tri->_len);
	tri->lines = 0;
	
	return tri;
}

void freeTriangle(struct Triangle *trian) {
	int i;
	for(i=0; i<trian->lines; ++i) {
		freeTriangleLine(trian->line[i]);
	}
	free(trian->line);
	free(trian);
}
void freeTriangleLine(struct TriangleLine *line) {
	free(line->node);
	free(line);
}

int addNode(struct TriangleLine *line, int value) {
	line->nodes++;
	
	if(line->nodes > line->_len) {
		line->_len += TRIANGLELINE_NODE_STEP;
		void *tmp = realloc(line->node,
				sizeof(struct TriangleNode)*line->_len);
		if(tmp == NULL)
			return FAILURE;
		line->node = tmp;
		
	}

	struct TriangleNode *node = &line->node[line->nodes-1];
/*
	if(line->nodes > 1) {
		node.left = line->node[line->nodes-2];
		line->node[line->nodes-2].right = &node;
	} else
		node.left = NULL;
	node.right = NULL;
*/
	node->value = value;
	node->childL = NULL;
	node->childR = NULL;
	//printf("\tValue:%d\n", line->node[line->nodes-1].value);
	
	return SUCCESS;
}

int addLine(struct Triangle *tri, struct TriangleLine *line) {
	if(tri->lines > 0)
		if(line->nodes != tri->line[tri->lines-1]->nodes +1)
			return FAILURE;

	tri->lines++;
	
	if(tri->lines > tri->_len) {
		tri->_len += TRIANGLE_LINE_STEP;
		void *tmp = realloc(tri->line,
				sizeof(struct TriangleLine *)*tri->_len);
		if(tmp == NULL)
			return FAILURE;
		tri->line = tmp;
		
	}
	
	tri->line[tri->lines-1] = line;
	
	if(tri->lines > 1) {
		int i;
		struct TriangleNode *nodesbefore = tri->line[tri->lines-2]->node;
		struct TriangleNode *nodes = line->node;
		for(i=0; i<line->nodes-1; ++i) {
			nodesbefore->childL = nodes; 
			nodesbefore->childR = nodes + 1;
			nodesbefore++; 
			nodes++; 
		}
	}
	
	return SUCCESS;
}
