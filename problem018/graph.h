#ifndef GRAPH_H
#define GRAPH_H

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define TRUE 1
#define FALSE 0

#define SUCCESS 0
#define FAILURE 1

#define GRAPH_ALL_INITIAL_SIZE 100
#define GRAPH_ALL_STEP 50

#define NODE_CHILD_INITIAL_SIZE 2
#define NODE_CHILD_STEP 2

#define MAX_COST (INT_MAX)

struct Node {
	char *name;
	int _len;
	int children;
	struct Node **child;
	int *weight;
	
	int _lenP;
	int parents;
	struct Node **parent;
	int *weightP;
	
	int cost;
	int visited;
};

struct Graph {
	int _len;
	int nodes;
	struct Node *start;
	struct Node **all;
};

struct Graph *newGraph(void);
struct Node *newNode(char *name);

void freeGraph(struct Graph *);
void freeNode(struct Node *);

int addNode(struct Graph *g, struct Node *node);
int addChild(struct Node *node, struct Node *child, int weight);

void printNode(struct Node *n);


#endif // GRAPH_H
