#include "graph.h"

struct Graph *newGraph(void) {
	struct Graph *g = malloc(sizeof(struct Graph));
	g->_len = GRAPH_ALL_STEP;
	g->start = NULL;
	g->all = malloc(sizeof(struct Node *) * g->_len);
	g->nodes = 0;

	return g;
}

void freeGraph(struct Graph *g) {
	int i;
	for(i=0; i<g->nodes; ++i) {
		freeNode(g->all[i]);
	}
	free(g->all);
	free(g);
}

struct Node *newNode(char *name) {
	struct Node *n = malloc(sizeof(struct Node));
	
	n->name = name;
	n->_len = NODE_CHILD_INITIAL_SIZE;
	n->children = 0;
	n->child = malloc(sizeof(struct Node *) * n->_len);
	n->weight = malloc(sizeof(int) * n->_len);
/******/
	n->_lenP = NODE_CHILD_INITIAL_SIZE;
	n->parents = 0;
	n->parent = malloc(sizeof(struct Node *) * n->_lenP);
	n->weightP = malloc(sizeof(int) * n->_lenP);
/******/
	n->cost = MAX_COST;
	n->visited = FALSE;

	return n;
}

void freeNode(struct Node *node) {
	free(node->child);
	free(node->weight);
	free(node);
}

int addNode(struct Graph *g, struct Node *node) {
	if(g->start == NULL)
		g->start = node;

	g->nodes++;
	if(g->_len < g->nodes) {
		g->_len += GRAPH_ALL_STEP;
		void *tmp = realloc(g->all, sizeof(struct Node *)*g->_len);
		if(tmp == NULL)
			return FAILURE;
		g->all = tmp;
	}

	g->all[g->nodes-1] = node;

	return SUCCESS;
}

int addChild(struct Node *node, struct Node *child, int weight) {
	node->children++;
	child->parents++;
	if(node->_len < node->children) {
		node->_len += NODE_CHILD_STEP;
		void *tmp = realloc(node->child, sizeof(struct Node *) * node->_len);
		if(tmp == NULL)
			return FAILURE;
		node->child = tmp;
		
		tmp = realloc(node->weight, sizeof(int) * node->_len);
		if(tmp == NULL)
			return FAILURE;
		node->weight = tmp;
	}
	
	if(child->_lenP < child->parents) {
		child->_lenP += NODE_CHILD_STEP;
		void *tmp = realloc(child->parent, sizeof(struct Node *) * child->_lenP);
		if(tmp == NULL)
			return FAILURE;
		child->parent = tmp;
		
		tmp = realloc(child->weightP, sizeof(int) * child->_lenP);
		if(tmp == NULL)
			return FAILURE;
		child->weightP = tmp;
	}
	
	node->child[node->children-1] = child;
	node->weight[node->children-1] = weight;
	
	child->parent[child->parents-1] = node;
	child->weightP[child->parents-1] = weight;

	return SUCCESS;
}

void printNode(struct Node *n) {
	printf("%s - %p:\n", n->name, n);
	printf("\tcost:%d\n", n->cost);
	printf("\tvisited:%s\n", (n->visited) ? "TRUE" : "FALSE");
	int i;
	for(i=0; i<n->children; ++i)
		printf("\t%p : %d\n", n->child[i], n->weight[i]);
}
