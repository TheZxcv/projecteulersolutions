#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "triangle.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

int main(void) {
	size_t len = 40;
	char *line = malloc(sizeof(char) * len);
	
	int line_size = 1;
	struct TriangleLine *tline = newTrianLine(line_size);
	struct Triangle *tri = newTriangle();

	
	while(getline(&line, &len, stdin) > 1) {
		char *tok = strtok(line, " \n");
		while (tok != NULL) {
			int w = atoi(tok);
			addNode(tline, w);
			tok = strtok(NULL, " \n");
		}
		line_size++;
		if(addLine(tri, tline) == FAILURE)
			printf("addLine: Warning\n");
		tline = newTrianLine(line_size);
		char *line = malloc(sizeof(char) * len);
	}

	int j, i = tri->lines-2;
	for(; i >= 0; --i) {
		tline = tri->line[i];
		for(j=0; j<tline->nodes; ++j) {
			tline->node[j].value += MAX(
					tline->node[j].childL->value,
					tline->node[j].childR->value
						);
		}
	}
	
	printf("Max:%d\n", tri->line[0]->node[0].value);

	return EXIT_SUCCESS;
}
