#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

#define LIMIT (2000000)

#define FALSE 0
#define TRUE 1

int isPrime(int n) {
	int lim = (int) ceil(sqrt(n));
	int i;
	if(n%2 == 0)
		return FALSE;
	for(i = 3; i <= lim; i += 2)
		if(n%i == 0)
			return FALSE;
	return TRUE;
}

int main(void) {
	uint64_t sum = 2;
	int num = 3;
	do {
		if(isPrime(num))
			sum += num;
		num++;
	} while(num < LIMIT);
	printf("Sum: %" PRIu64 "\n", sum);
	return EXIT_SUCCESS;
}
