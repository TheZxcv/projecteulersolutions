#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#define INITIAL_NAME_LEN 18
#define ARRAY_LEN 5000

struct Name {
	char *name;
	int score;
};

int compareName(const void *a, const void *b) {
	return strcmp(((struct Name *) a)->name, ((struct Name *) b)->name);
}

int main(void) {
	int len = ARRAY_LEN;
	struct Name *names = malloc(sizeof(struct Name)*len);
	int i = 0;
	char c;
	do {
		if(i >= len) {
			len += 100;
			names = realloc(names, sizeof(struct Name)*len);
		}
			
		while((c = getchar()) != '"');

		char *name = malloc(sizeof(char)*INITIAL_NAME_LEN);
		names[i].name = name;
		names[i].score = 0;
		/* FIXME: realloc string if longer */
		while((c = getchar()) != '"') {
			names[i].score += tolower(c) - 'a' + 1;
			*name++ = c;
		}
		*name = '\0';
		c = getchar();
		i++;
	} while(c == ',');

	len = i;
	qsort(names, len, sizeof(struct Name), compareName);
	long sum = 0;
	for(i=0; i<len; ++i) {
		names[i].score *= i+1;
		//printf("%d: name: %s, score: %d\n", i+1, names[i].name, names[i].score);
		sum += names[i].score;
	}
	printf("Sum scores: %ld\n", sum);
	return EXIT_SUCCESS;
}
