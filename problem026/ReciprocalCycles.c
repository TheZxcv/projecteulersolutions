#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LENGTH 1000

int main(void) {
	int *seq = malloc(sizeof(int)*LENGTH);
	int num, dem;
	int longest, Maxlen = 0;;

	for(dem = 7; dem < 1000; ++dem) {

		int pos=1;
		num=1;
		memset(seq, 0, sizeof(int)*LENGTH);
		do {loops++;
			num *= 10;
			num %= dem;
			if(seq[num])
				break;
			else	
				seq[num] = pos;
			pos++;
		} while(num != 0);
		if(pos-seq[num] > Maxlen) {
			Maxlen = pos-seq[num];
			longest = dem;
		}
	}

	printf("Longest recurring cyle 1/%d -> %d\n", longest, Maxlen);
	return EXIT_SUCCESS;
}
