#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0

#define NUM (600851475143)

int isPrime(int n) {
	int lim = (int) ceil(sqrt(n));
	int i;
	if(n%2 == 0)
		return FALSE;
	for(i = 3; i <= lim; i += 2)
		if(n%i == 0)
			return FALSE;
	return TRUE;
}

int main(void) {
	int n  = (int) ceil(sqrt(NUM));
	if(n%2 == 0)
		n--;

	while(n > 1) {
		if(NUM%n == 0)
			if(isPrime(n))
				break;
		n -= 2;
	}

	printf("Largest prime factor: %d\n", n);
	return EXIT_SUCCESS;
}
