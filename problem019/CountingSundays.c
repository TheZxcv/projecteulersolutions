#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TRUE 1
#define FALSE 0

#define REFERENCE_YEAR 1900
#define START_YEAR (1901)
#define END_YEAR (2000)

#define DAYS_IN_A_YEAR (365)

enum { MONDAY=0, TUESDAY, WEDNESDAY, THURSDAY,
	FRIDAY, SATURDAY, SUNDAY
	};
char *dayNames[7] = { "Monday", "Tuesday", "Wednesday", 
			"Thursday", "Friday", "Saturday", 
			"Sunday"};
char *getDayName(int day) {
	return dayNames[day];
}


enum { JANUARY=0, FEBRUARY, MARCH, APRIL,
	MAY, JUNE, JULY, AUGUST, SEPTEMBER,
	OCTOBER, NOVEMBER, DECEMBER
	};
char *monthNames[12] = {"January", "February", "March", "April",
			"May", "June", "July", "August", "September", 
			"October", "November", "December"
		};
int daysInAMonth[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
char *getMonthName(int month) {
	return monthNames[month];
}

/*
 * A leap year occurs on any year evenly divisible by 4, 
 * but not on a century unless it is divisible by 400.
 */
int isLeapYear(int year) {
	return (year%4 == 0 && year%100 != 0) ||
		(year%400 == 0);
}

/* 1 Jan 1900 was a Monday */
int dayFirstOfMonth(int year, int month, int leapYearsBefore) {
	int daysOffset = (year-REFERENCE_YEAR)*(DAYS_IN_A_YEAR%7) + leapYearsBefore;
	int i;
	for(i=0; i<month; ++i)
		daysOffset += daysInAMonth[i];
	if(month > 1)
		if(isLeapYear(year))
			daysOffset++;
	return daysOffset%7;
}

int main(void) {
//	int startDay = (DAYS_IN_A_YEAR + isLeapYear(1900))%7;
//	printf("%s\n", getDayName(startDay));

	int leapYears = 0;
	int SundaysOnFirst = 0;
	int year = START_YEAR;
	for(; year <= END_YEAR; ++year) {
		int month;
		for(month=0; month<12; ++month) {
			if(dayFirstOfMonth(year, month, leapYears) == SUNDAY)
				SundaysOnFirst++;
		}
		if(isLeapYear(year))
			leapYears++;
	}
	
	printf("There are %d Sundays on the first of the month "
		"between 1 Jan %d to 31 Dec %d\n", SundaysOnFirst,
		START_YEAR, END_YEAR);
	
	return EXIT_SUCCESS;
}
