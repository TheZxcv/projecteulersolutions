#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>


#define LIMIT (1000000)

#define INITIAL_SIZE (LIMIT)
#define STEP (10000)

int main(void) {
	int64_t size = INITIAL_SIZE;
	int64_t *lookup = calloc(INITIAL_SIZE, sizeof(int64_t)); 
	int64_t max = 1;
	int64_t Smax = 0;
	int64_t i;
	int64_t tmp, seq;
	for(i=1; i<LIMIT; ++i) {
		
		seq = 1;
		tmp = i;
		while(tmp > 1) {
			if(tmp%2 == 0)
				tmp /= 2;
			else 
				tmp = 3*tmp + 1;
			seq++;
			if(tmp < size && tmp > 1)
				if(lookup[tmp-1] != 0) {
					seq += lookup[tmp-1];
					tmp = 1;
				}
			//printf("Tmp: %d: seq %d\n", tmp, seq);
		}
		if(lookup[max-1] < seq) {
		//if(Smax < seq) {
			max = i;
			Smax = seq;
			//printf("Max: %"PRIu64": seq %"PRIu64"\n", max = i, seq);
		}
		lookup[i-1] = seq;
//		if(i%10000 == 0)
//			printf("progress %d%%\r", (int) floor(100.0*i/LIMIT));
	}
	printf("Max: %"PRIu64": seq %"PRIu64"\n", max, Smax);
	return EXIT_SUCCESS;
}
