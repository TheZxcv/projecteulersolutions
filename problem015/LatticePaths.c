#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

#define LIMIT (0x100000)
//Limit: 1048576
//Paths: 184756

int main(void) {
	int onesbit = ((int) ceil(log2l(LIMIT))) >> 1;
	uint64_t i, lim = LIMIT;
	uint64_t paths = 0;
	int perc = 0;
	printf("Limit: %" PRIu64 "\n", LIMIT);
	printf("Bits: %d\n", onesbit);
	for(i = 0; i < lim; ++i) {
		if(__builtin_popcount(i) == onesbit)
				paths++;
		/*if(perc+1 < (int) floor(100.0*i/LIMIT))
			printf("progress %d%%\r", perc = (int) floor(100.0*i/LIMIT));*/
	}
	
	printf("\nPaths: %"PRIu64"\n", paths);
	return EXIT_SUCCESS;
}
