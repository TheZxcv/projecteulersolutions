#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <inttypes.h>

int numDivisors(int n) {
	if(n == 1)
		return 1;
	int d = 2; /* one and itself */
	int lim = (int) ceil(sqrt(n));
	int i;
	for(i=2; i<lim; ++i) {
		if(n%i == 0)
			d += 2;
	}
	if(lim*lim == n)
		d--;
	return d;
}

int main(void) {
	int i = 1;
	int n = (i*(i+1))/2;
	int divs;
	int max = 0;
	do {
		n = (i*(i+1))/2;
		divs = numDivisors(n);
		if(max < divs)
			printf("Num: %d, %d, i:%d\n", n, max = divs,i);
		i++;
	} while(divs < 500);

	printf("Num: %d, %d\n", n, numDivisors(n));
	return EXIT_SUCCESS;
}
