#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT (1000)

int digits[10] = {
	4,	// zero
	3,	// one
	3,	// two
	5,	// three
	4,	// four
	4,	// five
	3,	// six
	5,	// seven
	5,	// eight
	4	// nine
};

int teens[10] = {
	3,	// ten
	6,	// eleven
	6,	// twelve
	8,	// thirteen
	8,	// fourteen
	7,	// fifteen
	7,	// sixteen
	9,	// seventeen
	8,	// eighteen
	8	// nineteen
};

int tens[10] = {
	0,	// padding
	3,	// ten
	6,	// twenty
	6,	// thirty
	5,	// forty
	5,	// fifty
	5,	// sixty
	7,	// seventy
	6,	// eighty
	6	// ninety
};

int and = 3; /* it goes between hundreds and tens */
int hundred = 7;
int thousand = 8;


int main(void) {
	int counter = 0;
	int i, tmp;
	for(i = 1; i<=LIMIT; ++i) {
		tmp = i%100;
		if((tmp > 0 && tmp < 10) || i == 0)
			counter += digits[tmp];
		else if(tmp > 10 && tmp < 20)
			counter += teens[tmp-10];
		else if(tmp != 0) {
			counter += tens[tmp/10];
			if(tmp%10 > 0)
				counter += digits[tmp%10];
		}
		tmp = i/100;
		if(tmp > 0) {
			if(tmp%10 > 0) {
				if(i%100 > 0)
					counter += and;
				counter += hundred;
				counter += digits[tmp%10];
			}
			if(tmp/10 > 0) {
				counter += thousand;
				counter += digits[tmp/10];
			}
		}
	}
	
	printf("Count: %d\n", counter);
	return EXIT_SUCCESS;
}
