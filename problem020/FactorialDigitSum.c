#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define MAX(a, b) (((a) < (b)) ? (b) : (a))

#define MAX_DIGITS_VALUE (999999999)
#define MAX_DIGITS (9)

struct DigitCluster {
	int digits;
	struct DigitCluster *left;
	struct DigitCluster *right;
};

struct Number {
	struct DigitCluster *first;
	struct DigitCluster *last;
	int len;
};

struct Number *newNumber(const char *str, int len) {
	char *string = malloc(sizeof(char) * len);
	memcpy(string, str, len);
	struct Number *num = malloc(sizeof(struct Number));
	num->len = len/MAX_DIGITS;
	num->len += (len%MAX_DIGITS > 0) ? 1 : 0;
	struct DigitCluster *digits = malloc(
				sizeof(struct DigitCluster)*(num->len));
	num->last = digits;
	char *tmp;	
	int i;
	for(i=1; i<=num->len; i++) {
		tmp = (string+len) - MAX_DIGITS*i;
		char c = *(tmp+MAX_DIGITS);
		//printf("c:%c\n", c);
		*(tmp+MAX_DIGITS) = '\0';
		if(tmp < string)
			tmp = string;
		if(i != 1)	
			digits->right = digits - 1;
		else
			digits->right = NULL;
		digits->left = digits + 1;
		digits->digits = atoi(tmp);
		//printf("%d\n", digits->digits);	
		tmp = (string+len) - MAX_DIGITS*i;
		*(tmp+MAX_DIGITS) = c;
		digits++;
	}
	digits--;
	digits->left = NULL;
	num->first = digits;
	free(string);
	return num;
	
}

struct Number *sumNumbers(const struct Number *a, const struct Number *b) {
	/* estimated size, it might be too little */
	int len = MAX(a->len, b->len);
	if(a->first->digits + b->first->digits > MAX_DIGITS_VALUE)
		len++;
	struct Number *num = malloc(sizeof(struct Number));
	num->len = len;
	struct DigitCluster *digits = malloc(
				sizeof(struct DigitCluster)*(num->len));
	num->last = digits;
	struct DigitCluster *Adigs = a->last;
	struct DigitCluster *Bdigs = b->last;
	
	int i, x, y, carry = 0;
	for(i=0; i<len; ++i) {
		x = y = 0;
		if(i < a->len) {
			x = Adigs->digits;
			Adigs = Adigs->left;
		}
		if(i < b->len) {
			y = Bdigs->digits;
			Bdigs = Bdigs->left;
		}
		x = x + y + carry;
		if(x > MAX_DIGITS_VALUE) {
			carry = x/(MAX_DIGITS_VALUE+1);
			x = x - carry*(MAX_DIGITS_VALUE+1);
		} else
			carry = 0;
		digits->digits = x;
		if(i != 0)
			digits->right = digits - 1;
		else
			digits->right = NULL;
		digits->left = digits + 1;
		digits++;
	}
	digits--;
	digits->left = NULL;
	num->first = digits;
	return num;
}

void printNumber(struct Number *n) {
	struct DigitCluster *dig = n->first;
	printf("%9d", dig->digits);
	dig = dig->right;
	while(dig != NULL) {
		printf(" %09d", dig->digits);
		dig = dig->right;
	}
	putchar('\n');
}

void *freeNumber(struct Number *n) {
	free(n->last);
	free(n);
}

int sumDigits(int num) {
	int sum = 0;
	while(num != 0) {
		sum += num%10;
		num /= 10;
	}
	return sum;
}

int main(void) {
	int i, j, lim = 100;
	struct Number *n = newNumber("2", 2);
	struct Number *tmp;
	for(i=3; i <= lim; ++i) {
		tmp = n;
		for(j=1; j<i; ++j) {
			tmp = sumNumbers(tmp, n);
		}
		free(n);
		n = tmp;
	}
	int sum = 0;
	struct DigitCluster *dig = n->first;
	while(dig != NULL) {
		sum += sumDigits(dig->digits);
		dig = dig->right;
	}
	printNumber(n);
	printf("Sum digits: %d\n", sum);
	return EXIT_SUCCESS;
}
