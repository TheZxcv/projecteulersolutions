#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT 4000000
#define SQRT5 (2.23606797749978969640917366873127623544)

double phi = (1.0 + SQRT5)/2.0;
int fib(int n) {
	int t1, t2;
	t1 = pow(phi, n);
	if(n%2 == 0)
		t2 = t1;
	else
		t2 = -t1;
	t2 = 1.0/t2;
	
	return (int) ceil((t1 - t2)/SQRT5);
}

int main(void) {
	int n = 1;
	int sum = 0;
	int tmp;

	while((tmp = fib(n)) < LIMIT) {
		printf("%d\n", tmp);
		if(tmp%2 == 0)
			sum += tmp;
		n++;
	}
	printf("Sum: %d\n", sum);

	return EXIT_SUCCESS;
}
