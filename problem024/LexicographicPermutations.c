#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <stdint.h>

#define USED 1
#define UNUSED 0
#define TRUE 1
#define FALSE 0
#define PERMUTATION 1000000
/* it must start with a 2 
 *
 * 10^6 - 2*9! = 274 240
*/

int fact_lookup[11] = {
		/* 0! */ 1,
		/* 1! */ 1,
		/* 2! */ 2,
		/* 3! */ 6,
		/* 4! */ 24,
		/* 5! */ 120,
		/* 6! */ 720,
		/* 7! */ 5040,
		/* 8! */ 40320,
		/* 9! */ 362880,
		/* 10! */ 3628800,
		};
/* factorial with lookup table */
int64_t fact(int n) {
	assert(n >= 0);
	int prod = 1;
	if(n < 11)
		return fact_lookup[n];
	else
		prod = fact_lookup[10];

	int i=10;
	for(; i<= n; ++i)
		prod *= i;
	return prod;
};

int perm(int *set, int len, int unused, char *res, int64_t target) {
	if(target == 0)
		return TRUE;

	int i = len - 1;
	int j = unused-1;
	int p = unused-1;
	for(; i >= 0; --i) {
		if(set[i] == UNUSED) {
			if(target - j*fact(p) > 0) {
				set[i] = USED;
				*res++ = '0' + i;
				return perm(set, len, p, res, target-j*fact(p));
			}
			--j;
		}
	}	
	return FALSE;
}

int main(void) {
	int set[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	char result[11];
	result[10] = '\0';
	perm(set, 10, 10, result, PERMUTATION);
	printf("%dth permutation: %s\n", PERMUTATION, result);
	return EXIT_SUCCESS;
}
