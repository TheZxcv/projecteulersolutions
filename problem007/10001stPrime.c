#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define TARGET 10001

#define FALSE 0
#define TRUE 1

int isPrime(int n) {
	int lim = (int) ceil(sqrt(n));
	int i;
	if(n%2 == 0)
		return FALSE;
	for(i = 3; i <= lim; i += 2)
		if(n%i == 0)
			return FALSE;
	return TRUE;
}

int main(void) {
	int num_pr = 1; /* skip 2 */
	int last = 1;
	do {
		last += 2;
		if(isPrime(last))
			num_pr++;
	} while(num_pr != TARGET);
	printf("%dst : %d\n", num_pr, last);
	return EXIT_SUCCESS;
}
