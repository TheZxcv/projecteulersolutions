#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <math.h>

#define LIMIT (100)

uint64_t partialSum(uint64_t n) {
	return (n*(n + 1)*(2*n + 1))/6;
}

int main(void) {
	uint64_t squaresSum = partialSum(LIMIT);
	uint64_t squareSum = (LIMIT*(LIMIT+1))/2;
	squareSum *= squareSum;
	printf("%" PRIu64, squareSum);
	printf(" - %" PRIu64, squaresSum);
	printf(" = %" PRIu64 "\n",  squareSum-squaresSum);
	return EXIT_SUCCESS;
}
