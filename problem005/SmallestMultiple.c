#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int gcd (int a, int b) {
	int c;
	while ( a != 0 ) {
		c = a; a = b%a;  b = c;
	}
	return b;
}

int main(void) {
	int fact = 2, i;
	for(i=3; i < 21; i++) {
		printf("i: %d\n", i);
		printf("\tq:%d\n", fact/i);
		printf("\tr:%d\n", fact%i);
		printf("\tgcd:%d\n", gcd(fact, i));
		if(fact%i != 0) {
			int gcdFI = gcd(fact, i);
			if(gcdFI == 1)
				fact *= i;
			else
				fact *= i/gcdFI;
		}
	}
	printf("Fact: %d\n", fact);
	return EXIT_SUCCESS;
}
